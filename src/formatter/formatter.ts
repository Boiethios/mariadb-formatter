import { equal, Group, Span, TokenStream, TokenTree } from "../lexer/tokens";
import { RawToken } from "../lexer/raw-token";

export type Indent = { name: 'Tab' } | { name: 'Spaces', num: number };

export type Case = 'Upper' | 'Lower' | 'Capitalized';

export function newCase(case_: string): Case {
    return case_ === 'Lower' || case_ === 'Capitalized'
        ? case_
        : 'Upper';
}

export interface Parameters {
    delimLinefeed: number
    indent: Indent
    keywordCase: Case
    lowerCaseIdents: boolean
    leadingComma: boolean
    linefeedAt: Set<string>
    maxSizeLine: number
    eol: string
    customKeywords: string[]
}

export function format_(tree: TokenStream, params: Parameters, level: number): string {
    let output = '';
    let lineSize = 0;
    let createTable = false;

    function append(...str: string[]) {
        output = output.concat(...str);
        lineSize = output.length - output.lastIndexOf('\n');
    }

    for (let i = 0; i < tree.length; ++i) {
        const cur = tree[i];
        const prev: TokenTree & Span | undefined = tree[i - 1];

        if (equal<TokenTree>({ name: 'Keyword', value: 'CREATE' }, cur, 'skip_uppercase')) {
            createTable = isCreateTable(tree.slice(i + 1));
        }
        // Whitespaces management:
        if (cur.name === 'Keyword' && params.linefeedAt.has(cur.value)
            || prev?.name === 'LineComment'
            || prev?.name === 'Delimiter'
            || prev?.name === 'ClosingComment' && cur.line !== prev?.line
            || cur.name === 'LineComment' && cur.line !== prev?.line) {
            append(params.eol.repeat(prev?.name === 'Delimiter' ? 1 + params.delimLinefeed : 1),
                padding(level, params));
        } else if (prev != null
            && (cur.name !== 'Delimiter' || cur.value !== ';')
            && cur.name !== 'Group'
            && (prev?.name !== 'Group' || prev?.end.name !== 'Delimiter')) {
            append(' ');
        }

        if (cur.name === 'Group') {
            // ToDo test with classic equality
            if (cur.start.name === 'LParen') {
                paren(cur, append, level, lineSize, params, createTable);
                createTable = false;
            } else if (equal(cur.start, { name: 'Keyword', value: 'SET' }, 'skip_uppercase')) {
                set(cur, append, level, params);
            } else {
                begin(cur, append, level, params);
            }
        } else {
            append(stringifyToken(cur, params));
        }
    }

    return output;
}


function paren(gr: Group, append: (...str: string[]) => void, level: number, lineSize: number, params: Parameters, createTable: boolean) {
    if (gr.sep == null) { return; }

    if (gr.value.length === 1 && createTable === false) {
        append(
            stringifyToken(gr.start, params),
            format_(gr.value[0], params, level + 1),
            stringifyToken(gr.end, params),
        );
    } else if (createTable === false && lineSize + groupSize(gr) <= params.maxSizeLine) {
        append(stringifyToken(gr.start, params));
        for (const [i, stream] of gr.value.entries()) {
            append(format_(stream, params, level + 1));
            if (gr.sep.name !== 'LitNum' && i !== gr.value.length - 1) {
                append(gr.sep.value, ' ');
            }
        }
        append(stringifyToken(gr.end, params));
    } else if (params.leadingComma) {
        append(params.eol, padding(level + 1, params), stringifyToken(gr.start, params), ' ');
        for (const [i, stream] of gr.value.entries()) {
            if (gr.sep.name !== 'LitNum' && i !== 0) {
                append(params.eol, padding(level + 1, params), gr.sep.value, ' ');
            }
            append(format_(stream, params, level + 1));
        }
        append(params.eol, padding(level + 1, params), stringifyToken(gr.end, params));
    } else {
        append(' ', stringifyToken(gr.start, params), params.eol, padding(level + 1, params));
        for (const [i, stream] of gr.value.entries()) {
            append(format_(stream, params, level + 1));
            if (gr.sep.name !== 'LitNum' && i !== gr.value.length - 1) {
                append(gr.sep.value, params.eol, padding(level + 1, params));
            }
        }
        append(params.eol, padding(level, params), stringifyToken(gr.end, params));
    }
}

function set(gr: Group, append: (...str: string[]) => void, level: number, params: Parameters) {
    if (gr.sep == null) { return; }

    if (gr.value.length === 1) {
        append(
            stringifyToken(gr.start, params),
            ' ',
            format_(gr.value[0], params, level + 1),
            stringifyToken(gr.end, params),
            params.eol, padding(level, params),
        );
    } else {
        append(params.eol, padding(level, params), stringifyToken(gr.start, params), ' ');
        for (const [i, stream] of gr.value.entries()) {
            append(format_(stream, params, level));
            if (gr.sep.name !== 'LitNum' && i !== gr.value.length - 1) {
                append(gr.sep.value, params.eol, padding(level, params), '    ');
            }
        }
        append(stringifyToken(gr.end, params));
    }
}

function begin(gr: Group, append: (...str: string[]) => void, level: number, params: Parameters) {
    append(
        params.eol, padding(level, params),
        stringifyToken(gr.start, params),
        params.eol, padding(level + 1, params),
        format_(gr.value[0], params, level + 1),
        params.eol, padding(level, params),
        stringifyToken(gr.end, params),
    );
}

/**
 * Get the number of characters of a group.
 * @param gr
 * @returns 
 */
function groupSize(gr: Group): number {
    return (
        gr.start.value.toString().length
        + gr.value
            .reduce((acc, val) => acc.concat(val), [])
            .map(item => item.name === 'Group' ? groupSize(item) : item.value.toString().length)
            .reduce((x, y) => x + y, 0)
        + gr.end.value.toString().length
    );
}

function padding(level: number, params: Parameters): string {
    if (params.indent.name === 'Tab') {
        return '\t'.repeat(level);
    } else {
        return ' '.repeat(params.indent.num * level);
    }
}

function stringifyToken(token: RawToken, params: Parameters): string {
    if (token.name === 'LitNum') {
        return token.value.toString();
    }

    if ((token.name === 'Ident' || token.name === 'LocalVar') && params.lowerCaseIdents) {
        return token.value.toLowerCase();
    }

    if (token.name !== 'Keyword') {
        return token.value;
    }

    switch (params.keywordCase) {
        case 'Capitalized':
            return token.value.charAt(0).concat(token.value.substring(1).toLowerCase());
        case 'Lower':
            return token.value.toLowerCase();
        case 'Upper':
            return token.value;
    }
}

function isCreateTable(tree: TokenStream): boolean {
    let i = 0;
    function skipComments() {
        while (tree[i]?.name === 'ClosingComment' || tree[i]?.name === 'LineComment') {
            ++i;
        }
    }
    skipComments();

    if (equal<TokenTree>(tree[i], { name: 'Keyword', value: 'OR' })) {
        skipComments();
        if (equal<TokenTree>(tree[i], { name: 'Keyword', value: 'REPLACE' }) === false) { return false; }
        skipComments();
        if (equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TABLE' })) { return true; }
        if (equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TEMPORARY' }) === false) { return false; }
        skipComments();
        return equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TABLE' });
    } else if (equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TEMPORARY' })) {
        skipComments();
        return equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TABLE' });
    }

    return equal<TokenTree>(tree[i], { name: 'Keyword', value: 'TABLE' });
}
