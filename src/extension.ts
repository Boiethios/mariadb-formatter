// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import { tokenize } from './lexer/lexer';
import { format_, Parameters, newCase, Indent } from "./formatter/formatter";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    if (context.extensionMode !== vscode.ExtensionMode.Production) {
        console.debug('Congratulations, your extension "mariadb-formatter" is now active!');
    }

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.languages.registerDocumentFormattingEditProvider('sql', {
        provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
            const fullDoc = new vscode.Range(new vscode.Position(0, 0), document.positionAt(document.getText().length));
            const editorConfig = vscode.workspace.getConfiguration('editor', vscode.window.activeTextEditor?.document.uri);
            const extConfig = vscode.workspace.getConfiguration('mariadb-formatter', vscode.window.activeTextEditor?.document.uri);

            const indent: Indent = editorConfig.get('insertSpace', true)
                ? { name: 'Spaces', num: editorConfig.get('tabSize', 4) }
                : { name: 'Tab' };

            const params = {
                delimLinefeed: extConfig.get('delimLinefeed', 2),
                indent,
                keywordCase: newCase(extConfig.get('keywordCase', 'Upper')),
                lowerCaseIdents: extConfig.get('lowerCaseIdents', false),
                leadingComma: extConfig.get('leadingComma', true),
                linefeedAt: new Set(extConfig.get<string[]>('linefeedAt', [])),
                maxSizeLine: editorConfig.get('keywordCase', 100),
                eol: document.eol === vscode.EndOfLine.LF ? '\n' : '\r\n',
                customKeywords: extConfig.get<string[]>('customKeywords', []),
            };

            return [vscode.TextEdit.replace(fullDoc, formatMysql(document.getText(), params))];
        }
    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() { }

export function formatMysql(input: string, params: Parameters): string {
    let tokens = tokenize(input, params.customKeywords);
    if (tokens.success === false) {
        vscode.window.showInformationMessage(`Cannot scan the file`, `Error: ${tokens.error}, 'at' ${tokens.span.line}:${tokens.span.column}`);
        throw new Error(`error: ${tokens.error}, 'at' ${tokens.span.line}:${tokens.span.column}`);
    }

    try {
        let formatted = format_(tokens.tokens, params, 0);

        return formatted;
    } catch (e) {
        vscode.window.showInformationMessage(`Cannot format the file`, `Error: ${e}`);
        throw e;
    }
}
