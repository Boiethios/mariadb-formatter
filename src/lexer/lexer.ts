import { TokenStream, Span, equal, TokenTree } from './tokens';
import { RawToken, tokenizeRaw } from "./raw-token";
import { mariadbKeywords } from "./keywords";

export interface Ok { success: true, tokens: TokenStream }
export interface Err { success: false, span: Span, error: string }

export function tokenize(input: string, customKeywords: string[]): Ok | Err {
    let keywords = new Set([
        'BEGIN',
        'END',
        'DELIMITER',
        ...mariadbKeywords,
        ...[...customKeywords].map(k => k.toUpperCase()),
    ]);

    let res = tokenizeRaw(input, keywords);
    if (res.name === 'Error') {
        return { success: false, span: { line: res.line, column: res.column }, error: 'invalid token' };
    }

    let gr = group(res.tokens, null, 0);
    if (gr.success === false) {
        return gr;
    }

    return { success: true, tokens: gr.streams[0] };
}

const groupTokens: { start: RawToken, end: RawToken, sep: RawToken | null }[] = [
    { start: { name: 'LParen', value: '(' }, end: { name: 'RParen', value: ')' }, sep: { name: 'Comma', value: ',' } },
    { start: { name: 'Keyword', value: 'BEGIN' }, end: { name: 'Keyword', value: 'END' }, sep: null },
    { start: { name: 'Keyword', value: 'SET' }, end: { name: 'Delimiter', value: ';' }, sep: { name: 'Comma', value: ',' } },
];

/**
 * Return a newly created group from raw tokens.
 * @param input The raw tokens. 
 * @param groupType The start, end and sep token used for this group. An index for `group_token`.
 * @param index The index for the tokens array (current place).
 * @returns 
 */
function group(input: (Span & RawToken)[], groupType: number | null, index: number)
    : { success: true, streams: TokenStream[], skip: number } | Err {

    let streams: TokenStream[] = [];
    let stream: TokenStream = [];
    let skip = index === 0 ? 0 : 1;
    let i: number;

    while (input[index + skip] != null) {
        const cur = input[index + skip];
        const sep = groupType == null ? null : groupTokens[groupType].sep;

        // check for the end of group
        if (groupType != null && equal(cur, groupTokens[groupType].end)) {
            ++skip;
            streams.push(stream);
            return { success: true, streams, skip };
        }
        // check for the separator
        else if (sep != null && equal(cur, sep)) {
            ++skip;
            streams.push(stream);
            stream = [];
        }
        // check for a new group
        else if ((i = groupTokens.findIndex(elem => equal(cur, elem.start))) !== -1) {
            let gr = group(input, i, index + skip);
            if (gr.success === false) {
                return gr;
            }

            stream.push({
                name: 'Group',
                value: gr.streams,
                ...groupTokens[i],
                line: cur.line,
                column: cur.column,
            });
            skip += gr.skip;
        }
        // check for an invalid end token
        else if (cur.name !== 'Delimiter' && groupTokens.findIndex(elem => equal(cur, elem.end)) !== -1) {
            return { success: false, span: { line: cur.line, column: cur.column }, error: 'invalid end token' };
        }
        else {
            if (cur.name === 'LParen' || cur.name === 'RParen' || cur.name === 'Comma') {
                console.log(cur);
                throw new Error(`Should not get this token at this moment`);
            }
            stream.push(cur);
            ++skip;
        }
    }

    if (index === 0) { // Outer fake group
        return { success: true, streams: [stream], skip };
    }
    return { success: false, span: input[input.length - 1], error: 'missing end token' };
}
