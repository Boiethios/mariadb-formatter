import { RawToken } from "./raw-token";

export type TokenStream = (Span & TokenTree)[];

export interface Span {
    line: number
    column: number
}

export type TokenTree =
    | Group
    | ClosingComment
    | LineComment
    | Delimiter
    | Keyword
    | Ident
    | LocalVar
    | LitStr
    | LitNum
    | SymEqual;

export function equal<T extends RawToken | TokenTree>(a: T | undefined | null, b: T | undefined | null, skip?: 'skip_uppercase'): boolean {
    if (a == null || b == null) {
        return false;
    }
    if (skip === 'skip_uppercase') {
        return a.name === b.name && a.value.toString() === b.value.toString();
    }
    return a.name === b.name && a.value.toString().toUpperCase() === b.value.toString().toUpperCase();
}

export interface Group {
    name: 'Group'
    value: TokenStream[]
    start: RawToken
    end: RawToken
    sep: RawToken | null
}

export interface ClosingComment { name: 'ClosingComment', value: string }
export interface LineComment { name: 'LineComment', value: string }

export interface Delimiter { name: 'Delimiter', value: string }

export interface Keyword { name: 'Keyword', value: string }
export interface Ident { name: 'Ident', value: string }
export interface LocalVar { name: 'LocalVar', value: string }

export interface LitStr { name: 'LitStr', value: string }
export interface LitNum { name: 'LitNum', value: number }

export interface SymEqual { name: 'SymEqual', value: string }

export interface Comma { name: 'Comma', value: string }
export interface LParen { name: 'LParen', value: string }
export interface RParen { name: 'RParen', value: string }
