import {
    ClosingComment, LineComment, Delimiter, Keyword, Ident, LocalVar, LitStr, LitNum, SymEqual, Comma, LParen, RParen,
    Span,
    equal
} from "./tokens";

export type RawToken =
    | ClosingComment
    | LineComment
    | Delimiter
    | Keyword
    | Ident
    | LocalVar
    | LitStr
    | LitNum
    | SymEqual
    | Comma
    | LParen
    | RParen;


type Context = Readonly<{
    input: string;
    index: number;
    delimiter: string | null;
}>;

interface Error { name: 'Error' }
interface Eoi { name: 'Eoi' }

type Result = (RawToken & Span) | Eoi | Error;

export function tokenizeRaw(input: string, keywords: Set<string>): { name: 'TokenSteam' } & { tokens: (Span & RawToken)[] } | { name: 'Error' } & Span {

    /**
     * Returns whether a new delimited will be defined.
     * @param token the last token parsed
     */
    function nextIsDelimiter(token: Result): boolean {
        return token.name === 'Keyword' && token.value === 'DELIMITER';
    }

    let ctx = { input, index: 0, delimiter: null as (string | null) };
    let result: Result = { name: 'Eoi' };
    let stream: (Span & RawToken)[] = [];

    // Special case: in case of the keyword 'DELIMITER', the lexicon will change.
    // Setting `delimiter` to true allows to handle this change.
    while ({ ctx, result } = next(ctx, keywords, nextIsDelimiter(result))) {
        if (result.name === 'Error') {
            return { ...span(ctx), name: 'Error' };
        } else if (result.name === 'Eoi') {
            break;
        }

        stream.push(result);
    }

    return { tokens: stream, name: 'TokenSteam' };
}

/**
 * Returns the next token.
 * @param ctx The lexing context.
 * @param delimiter Allows to handle a weird special case: the dynamic change of delimiter. If true, will set a new delimiter.
 * @returns The new context and: the new parsed token, or 'end of input', or an error
 */
function next(ctx: Context, keywords: Set<string>, delimiter: boolean): { ctx: Context, result: Result } {
    ctx = skipWhitespace(ctx);
    let cur = ctx.input.substring(ctx.index);
    let s: string | null;

    function regex(ctx: Context, re: RegExp): string | null {
        return re.exec(ctx.input.substring(ctx.index))?.[0] ?? null;
    }

    function skipWhitespace(ctx: Context): Context {
        let skip = /^\s*/.exec(ctx.input.substring(ctx.index))?.[0].length ?? 0;

        return { ...ctx, index: ctx.index + skip };
    }

    function nextCtx(ctx: Context): Context {
        return { ...ctx, index: ctx.index + (s?.length ?? 0) };
    }

    if ((s = regex(ctx, /^CONTAINS\s+SQL/i)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Keyword', value: 'CONTAINS SQL' } } };
    } else if ((s = regex(ctx, /^NO\s+SQL/i)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Keyword', value: 'NO SQL' } } };
    } else if ((s = regex(ctx, /^READS\s+SQL\s+DATA/i)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Keyword', value: 'READS SQL DATA' } } };
    } else if ((s = regex(ctx, /^MODIFIES\s+SQL\s+DATA/i)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Keyword', value: 'MODIFIES SQL DATA' } } };
    } else if ((s = regex(ctx, /^\/\*.*\*\//)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'ClosingComment', value: s } } };
    } else if ((s = regex(ctx, /^--.*/)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'LineComment', value: s } } };
    } else if (delimiter && (s = regex(ctx, /^[^\s]+/)) != null) {
        // new delimiter
        return { ctx: { ...nextCtx(ctx), delimiter: s === ';' ? null : s }, result: { ...span(ctx), ...{ name: 'Delimiter', value: s } } };
    } else if ((s = cur[0]) === ';' || (s = ctx.delimiter) != null && cur.startsWith(s)) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Delimiter', value: s } } };
    } else if ((s = regex(ctx, /^'(''|[^'])*'/)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'LitStr', value: s } } };
    } else if ((s = regex(ctx, /^[\d.]+/)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'LitNum', value: +s } } };
    } else if ((s = regex(ctx, /^\w+/)) != null) {
        if (keywords.has(s.toUpperCase())) {
            return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Keyword', value: s.toUpperCase() } } };
        }
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Ident', value: s } } };
    } else if ((s = regex(ctx, /^@\w+/)) != null) {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'LocalVar', value: s } } };
    } else if ((s = cur[0]) === '=') {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'SymEqual', value: s } } };
    } else if ((s = cur[0]) === '(') {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'LParen', value: s } } };
    } else if ((s = cur[0]) === ')') {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'RParen', value: s } } };
    } else if ((s = cur[0]) === ',') {
        return { ctx: nextCtx(ctx), result: { ...span(ctx), ...{ name: 'Comma', value: s } } };
    } else if (ctx.index === ctx.input.length) {
        return { ctx: ctx, result: { name: 'Eoi' } };
    } else {
        return { ctx: ctx, result: { name: 'Error' } };
    }
}

/**
 * Returns the current line and column from the context.
 * @param ctx the lexing context
 * @returns the line/column information
 */
function span(ctx: Context): Span {
    let line = 1;
    let column = 1;
    let str = ctx.input;
    let index = ctx.index;

    for (let i = 0; i < index; ++i, ++column) {
        if (str[i] === '\n') {
            ++line;
            column = 0;
        }
    }

    return { line, column };
}
