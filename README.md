# mariadb-formatter

A MariaDB formatter for VSCode.

The extension is [available for downloading](https://gitlab.com/Boiethios/mariadb-formatter/-/releases). Note that it's an alpha version that is not guaranteed to work correctly.

## Features

Its main feature compared to the other existing formatters is that it handle correctly the `DELIMITER` behavior.

## Extension Settings

This extension contributes the following settings:

* `mariadb-formatter.delimLinefeed`: the number of additional linefeed after a delimiter
* `mariadb-formatter.keywordCase`: how the keyword are formatted
* `mariadb-formatter.lowerCaseIdents`: if the idents must be lowercased
* `mariadb-formatter.leadingComma`: where is the comma in a group (leading/trailing)
* `mariadb-formatter.linefeedAt`: the list of keyword after which there will be a linefeed
* `mariadb-formatter.customKeywords`: the list of custom keywords

## Changelogs

### 0.0.2

- Parameter `lowerCaseIdents` added.
- Bug correction: comments and strings were lowercased.
- The single-line comment is kept on the same line.
